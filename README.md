# C Operating System
This project bundles all the assignments, Shavit and I received in the Operating Systems course.

## Create Your Own Work Environment 
[Linux Virtual Machine Setup](./docs/linux-setup.md)<br/>
[VS Code Setup](./docs/vscode-setup.md)

## Exrecises
[Exrecise 1](./docs/exercise1/README.md)<br/>
[Exrecise 2](./docs/exercise2/README.md)<br/>




