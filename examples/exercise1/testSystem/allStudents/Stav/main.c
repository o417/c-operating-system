#include<stdio.h>

// Correct!

int main(int argc, char * argv[]) {
    int opr;   
    int num1, num2;

    do {
        printf("Please enter operation");
        scanf("%d", &opr);
        switch (opr) {
            case 1:
                printf("Please enter two numbers");
                scanf("%d %d", &num1, &num2);
                printf("The sum is %d\n", num1+num2);
                break;
            case 4:
                printf("Bye");
                break;
        }
    } while(opr!=4);

    return 0;
}