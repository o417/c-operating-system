#include<stdio.h>

// Incorrect!
// Did distance instead of sum

int main(int argc, char * argv[]) {
    int opr;   
    int num1, num2;

    do {
        printf("Please enter operation\n");
        scanf("%d", &opr);
        switch (opr) {
            case 1:
                printf("Please enter two numbers\n");
                scanf("%d %d", &num1, &num2);
                printf("The distance is %d\n", num1-num2);
                break;
            case 4:
                printf("Bye");
                break;
        }
    } while(opr!=4);

    return 0;
}