

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <fcntl.h>
#define SNPRINTF_MAX_BUFFER_SIZE 285
#define DIRNET_D_NAME_MAX_SIZE 256
#define RESULT_FILENAME "./results.csv"
#define STUDENT_OUTPUT_FILENAME "./studentOutput.txt"
#define COMPERATOR_PROGRAM_NAME "./comp.out"
#define STUDENT_EXE_FILENAME "main.exe"
#define STUDENT_PROGRAM_ARGC 1
#define FILES_COMPERATOR_ARGC 3
#define STUDENT_MAX_GRADE 100
#define STUDENT_MIN_GRADE 0

#define STRINGIFY(x) #x // Macro to convert argument to string
#define TOSTRING(x) STRINGIFY(x) // Helper macro to expand the value before converting to string


char * readLine(int fd);
bool isConfigFileInvalid(char * filename);
char * getStudProgramFullPath(char * sudentsDir, char * studentName, char * execFileName);
char ** getArgsToStudProgram(char *studExecFileName);
char ** getArgsToFileComparator(char *compExecFileName, char * studOutputFileName,char * expectedOutputFileName);
char * getStudRowResult(char * studentName, int comparisonStatus);

int main(int argc, char * argv[])
{
	char error_msg[SNPRINTF_MAX_BUFFER_SIZE];
	char *progName;
    int comparisonStatus;
	struct dirent * aStudDir;
	int pidRunStud;
	int pidRunComp;
	DIR * studentsDir = NULL;
	char ** argsToFileComparator = NULL;
    char ** argsToStudProgram = NULL;
	char * studentResultRow = NULL;
	// Files files decriptor
	int configFd;
	int resultFd;
	int inputFd;
	int studOutputFd;
	// names from configuration file
	char * studetnsDirName = NULL;
	char * inputFileName = NULL;
	char * expectedOutputFileName = NULL;

	// checking validity of the given arguments
	if(argc != 2)
	{
		perror("The program needs exactly one arguments\n");
		exit(1);
	}

	if(!isConfigFileInvalid(argv[1])) {
		perror("The config file is invalid\n");
		exit(2);
	}

	// Open config file for read
	configFd = open(argv[1], O_RDONLY);
    if(configFd < 0)
    {
        perror("Error when opening the given file\n");
        exit(3);
    }

	// Get studetns directory
	studetnsDirName = readLine(configFd);
	if(studetnsDirName == NULL)
    {
    	perror("Error when reading from the given file\n");
		close(configFd);
    	exit(4);
    }

	studentsDir = opendir(studetnsDirName);
    if(studentsDir == NULL)
    {
    	perror("Error when opening the directory in the first line of the given file\n");
    	free(studetnsDirName);
		close(configFd);
    	exit(5);
    }

	//creating the results.csv file
	resultFd = open(RESULT_FILENAME, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (resultFd < 0) {
		perror("Error when creating the results.csv file\n");
		free(studetnsDirName);
		closedir(studentsDir);
		close(configFd);
		exit(6);
	}

	if (fchmod(resultFd, 0644) < 0) {
    	perror("Failed to set file permissions to results.csv file");
    	close(resultFd);
		free(studetnsDirName);
		closedir(studentsDir);
		close(configFd);
    	exit(7);
	}

	// Read the INPUT file name from configuration file
	inputFileName = readLine(configFd);
	if(inputFileName == NULL)
	{
		perror("Error when reading from the given file\n");
		free(studetnsDirName);
		closedir(studentsDir);
		close(configFd);
		exit(8);
	}

	// Read the OUTPUT file name from configuration file
	expectedOutputFileName = readLine(configFd);
	if (expectedOutputFileName == NULL) {
		perror("Error when reading from the given file\n");
		free(studetnsDirName);
		free(inputFileName);
		closedir(studentsDir);
		close(configFd);
		exit(9);
	}

	// Executing each student's program and giving them a grade
    while(1) {
		// Read a student dir from the students directory
		do {
			aStudDir = readdir(studentsDir);
		} while(aStudDir != NULL && aStudDir->d_name[0] == '.'); // inorder to skip the  ./ and ../ default directoris
		
		if(aStudDir == NULL) { // No more students...
			break;
		}

		pidRunStud = fork();
		// father process, on fork faliure
		if(pidRunStud < 0) {
			perror("Failed to create a child process\n");
			free(studetnsDirName);
			free(expectedOutputFileName);
    		free(inputFileName);
			close(resultFd);
			closedir(studentsDir);
			close(configFd);
    		exit(10);
		}

		// child process - changes STDOUT to a new file and executes a students program (so it's output is in the file)
		if(pidRunStud == 0) { 
			
			progName = getStudProgramFullPath(studetnsDirName, aStudDir->d_name, STUDENT_EXE_FILENAME);
			if(progName == NULL) {
				perror("Error when geting the student program full path name\n");
				free(expectedOutputFileName);
    			free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
				exit(11);
			}
			free(studetnsDirName);

			argsToStudProgram = getArgsToStudProgram(progName);
			if(argsToStudProgram == NULL) {
				perror("Error when getting params to student program\n");
				free(expectedOutputFileName);
    			free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
				exit(12);
			}
			
			studOutputFd = open(STUDENT_OUTPUT_FILENAME, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    		if(studOutputFd < 0) {
    			perror("Failed to open the student output file\n");
				free(expectedOutputFileName);
    			free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
    			exit(13);
    		}

			inputFd = open(inputFileName, O_RDONLY);
    		if(inputFd < 0) {
    			perror("Failed to open the input file\n");
				close(studOutputFd);
				free(expectedOutputFileName);
    			free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
    			exit(14);
    		}
			
			// ----- I/O redirection -------
			dup2(studOutputFd, STDOUT_FILENO );
			dup2(inputFd, STDIN_FILENO);
			// ------ I/O redirection -------
			free(expectedOutputFileName);
			free(inputFileName);
			close(resultFd);
			closedir(studentsDir);
			close(configFd);
			execvp(progName, argsToStudProgram);
			/* an error in execvp accured */
			// snprintf - can write to buffer between 30 to 285
			snprintf(error_msg, sizeof(error_msg), "Failed to execute %s's program\n", aStudDir->d_name);
			perror(error_msg);
    		perror("");
    		free(progName);
			free(argsToStudProgram[0]);
			free(argsToStudProgram);
			close(inputFd);
    		close(studOutputFd);
    		exit(15);
    		/* an error in execvp accured */
		}
		// father process - compares between the students output to the correct output
		else {

			// if a child process exited with an error code then exit from the father process
			
			if(wait(NULL) == -1){
				perror("An Error accured in a child process that was ment to execute a student's program\n");
				free(expectedOutputFileName);
				free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
				exit(16);
			}
			pidRunComp = fork();
			// father process, on fork faliure
			if (pidRunComp < 0)
			{
				perror("Failed to create a child process\n");
				free(expectedOutputFileName);
				free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);
				exit(17);
			}
			// child process
			if (pidRunComp == 0) {
				// free and close unneeded resources
				free(inputFileName);
				close(resultFd);
				closedir(studentsDir);
				close(configFd);

				argsToFileComparator = getArgsToFileComparator(COMPERATOR_PROGRAM_NAME, STUDENT_OUTPUT_FILENAME, expectedOutputFileName);
				free(expectedOutputFileName);
				
				if (argsToFileComparator == NULL) {
					perror("Error when getting params to file comperator\n");
					exit(18);
				}
				
				// Asuming the comp.out is in the runing dir
				execvp(COMPERATOR_PROGRAM_NAME, argsToFileComparator);
				/* an error in execvp accured */
				perror("Failed to execute the file comparator\n");
				free(argsToFileComparator[2]);
				free(argsToFileComparator[1]);
				free(argsToFileComparator[0]);
				free(argsToFileComparator);
				exit(19);
				/* an error in execvp accured */
			}

			else // father process
			{
				if (wait(&comparisonStatus) == -1)
				{
					perror("An Error accured in a child process that was ment to compare files\n");
					free(expectedOutputFileName);
					free(inputFileName);
					close(resultFd);
					closedir(studentsDir);
					close(configFd);
					exit(20);
				}
				if (!WIFEXITED(comparisonStatus))
				{
					perror("the child process that was ment to compare files exited unnormally\n");
					free(expectedOutputFileName);
					free(inputFileName);
					close(resultFd);
					closedir(studentsDir);
					close(configFd);
					exit(21);
				}

				studentResultRow = getStudRowResult(aStudDir->d_name, comparisonStatus);
				if(studentResultRow == NULL) {
					perror("Error when getting student row result\n");
					free(expectedOutputFileName);
					free(inputFileName);
					close(resultFd);
					closedir(studentsDir);
					close(configFd);
					exit(22);
				}

				if (write(resultFd, studentResultRow, strlen(studentResultRow) * sizeof(char)) != strlen(studentResultRow) * sizeof(char))
				{
					free(studentResultRow);
					free(expectedOutputFileName);
					free(inputFileName);
					close(resultFd);
					closedir(studentsDir);
					close(configFd);
					exit(23);
				}
				free(studentResultRow);
			}
		}
	}

	free(studetnsDirName);
	free(expectedOutputFileName);
	free(inputFileName);
	close(resultFd);
    closedir(studentsDir);
    close(configFd);


	if (unlink(STUDENT_OUTPUT_FILENAME) != 0) {
        perror("Failed to remove the student output file\n");
		exit(24);
    }

    exit(0);
}

bool isConfigFileInvalid(char * filename) {
	struct stat st;
	if(stat(filename, &st) == -1)
	{
		perror("Error when invoking stat\n");
        return false;
	}
	if (!S_ISREG(st.st_mode))
    {
        perror("The given Path is not of a file\n");
        return false;
    }
    if(access(filename, R_OK) != 0)
    {
    	perror("This program doesn't have the permission to read from the given file\n");
        return false;
    }
    return true;
}


// reads a string till the '\n' character from a given stream
char * readLine(int fd) {
	int errCheck;
	char * line = malloc(sizeof(char));
	char * temp = NULL;
	int size = 1;
	if (line == NULL) {
		perror("Error allocating memory\n");
		return NULL;
	}

	while (1) {
		errCheck = read(fd, line + size - 1, sizeof(char));
		if (errCheck < 0) {
			perror("Error reading from file\n");
			free(line);
			return NULL;
		}
		if (line[size - 1] == '\n') {
			line[size - 1] = '\0';
			break;
		}
		temp = realloc(line, (size + 1) * sizeof(char));
			if (temp == NULL) {
				perror("Error allocating memory\n");
				free(line);
				return NULL;
			}
		line = temp;
		size++;
	}

	return line;
}

char * getStudProgramFullPath(char * sudentsDir, char * studentName, char * execFileName){
	char progName[DIRNET_D_NAME_MAX_SIZE + 3] = { 0 };

	if(sudentsDir == NULL || studentName == NULL || execFileName == NULL){
		perror("Failed to build student full path to his executable program\n");
		return NULL;
	}

	strcat(progName+ strlen(progName) , sudentsDir);
	strcat(progName + strlen(progName), "/");
	strcat(progName + strlen(progName), studentName);
	strcat(progName + strlen(progName), "/");
	strcat(progName + strlen(progName), execFileName);

	return strdup(progName);
}

char ** getArgsToStudProgram(char *studExecFileName) {
	char ** argsToStudProgram = NULL;

	if(studExecFileName == NULL) {
		perror("Failed to build params to file student program\n");
		return NULL;
	}

	argsToStudProgram = malloc((STUDENT_PROGRAM_ARGC + 1) * sizeof(char *));
	if (argsToStudProgram == NULL) {
		perror("Failed to allocate memory for student program's params\n");
		return NULL;
	}

	argsToStudProgram[0] = strdup(studExecFileName);
	if (argsToStudProgram[0] == NULL) {
		perror("Failed to allocate memory for student program's params\n");
		free(argsToStudProgram);
		return NULL;
	}

	// Null Termination - the last argument of the args array should be null
	argsToStudProgram[1] = NULL;
	return argsToStudProgram;
}


char ** getArgsToFileComparator(char *compExecFileName, char * studOutputFileName,char * expectedOutputFileName){
		char ** argsToFileComparator = NULL;

		if(compExecFileName == NULL || studOutputFileName == NULL || expectedOutputFileName == NULL) {
			perror("Failed to build params to file comperator\n");
			return NULL;
		}

		argsToFileComparator = malloc((FILES_COMPERATOR_ARGC+1) * sizeof(char *));
		if (argsToFileComparator == NULL) {
			perror("Failed to allocate memory for files comperator's params\n");
			return NULL;
		}

		argsToFileComparator[0] = strdup(compExecFileName);
		if (argsToFileComparator[0] == NULL) {
			perror("Failed to allocate memory for files comperator's params\n");
			free(argsToFileComparator);
			return NULL;
		}

		argsToFileComparator[1] = strdup(studOutputFileName);
		if (argsToFileComparator[1] == NULL) {
			perror("Failed to allocate memory for files comperator's params\n");
			free(argsToFileComparator[0]);
			free(argsToFileComparator);
			return NULL;
		}

		argsToFileComparator[2] = strdup(expectedOutputFileName);
		if (argsToFileComparator[2] == NULL) {
			perror("Failed to allocate memory for files comperator's params\n");
			free(argsToFileComparator[0]);
			free(argsToFileComparator[1]);
			free(argsToFileComparator);
			return NULL;
		}
		// Null Termination - the last argument of the args array should be null
		argsToFileComparator[3] = NULL;
		return argsToFileComparator;
}


char * getStudRowResult(char * studentName, int comparisonStatus) {
	char studentResult[DIRNET_D_NAME_MAX_SIZE + 5] = { 0 };

	if(studentName == NULL) {
		perror("Failed to get student result\n");
		return NULL;
	}

	strcat(studentResult, studentName);
	strcat(studentResult, ",");

	if(WEXITSTATUS(comparisonStatus) == 2) { // files are identical
		strcat(studentResult, TOSTRING(STUDENT_MAX_GRADE));
	}

	else{
		strcat(studentResult, TOSTRING(STUDENT_MIN_GRADE));
	}

	strcat(studentResult, "\n");

	return strdup(studentResult);
}
	