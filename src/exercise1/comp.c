//Ohad Omrad ID: <>
//Shavit <> ID: <>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char * argv[])
{
    int retVal;
    int fd1, fd2;
    char tav1;
    char tav2;
    struct stat stat1, stat2;

    if(argc != 3) {
        perror("The program needs exactly 2 arguments\n");
        exit(1);
    }
    if(stat(argv[1],&stat1) == -1) {
        perror("Error when invoking stat\n");
        exit(1);
    }
    if(stat(argv[2],&stat2) == -1) {
        perror("Error when invoking stat\n");
        exit(1);
    }
    if (!S_ISREG(stat1.st_mode)) {
        perror("Path 1 is not a file");
        exit(1);
    }
    if (!S_ISREG(stat2.st_mode)) {
        perror("Path 2 is not a file");
        exit(1);
    }
    if(stat1.st_size != stat2.st_size) {
        exit(1);
    }

    fd1 = open(argv[1], O_RDONLY);
    if(fd1 < 0) {
        perror("Error in opening the first file");
        exit(1);
    }

    fd2 = open(argv[2], O_RDONLY);
    if(fd2 < 0) {
        perror("Error in opening the second file");
        exit(1);
    }

    do {
        retVal = read(fd1, &tav1, sizeof(char));
        if(retVal < 0) {
            perror("Error in reading the first file");
            close(fd1);
            close(fd2);
            exit(1);
        }
        if(retVal == 0) {
            break;
        }
        retVal = read(fd2, &tav2, sizeof(char));
        if(retVal < 0)
        {
            perror("Error in reading the second file");
            close(fd1);
            close(fd2);
            exit(1);
        }
        if(tav1 != tav2) {
            close(fd1);
            close(fd2);
            exit(1);
        }
    } while(1);

    close(fd1);
    close(fd2);
    exit(2);
}