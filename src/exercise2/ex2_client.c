#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/file.h>

#define ARGC_VALUE 5
#define SNPRINTF_MAX_BUFFER_SIZE 285
#define TO_SERVER_FILE "toServer.txt"
#define NEW_LINE "\n"
#define MAX_PID_LEN 10
#define SERVER_SIGNAL 1
#define CLIENT_SIGNAL 2
#define NUM_CONNECTIONS_ATTEMPTS 10
#define TIMEOUT 30
#define PLUS_OPERATION 1
#define MINUS_OPERATION 2
#define MUL_OPERATION 3
#define DIV_OPERATION 4
#define MAX_EXPRESSION_LEN 256
#define MAX_SYMBOL_LEN 8

bool isNumber(const char *str);
char * getResponseFileName(pid_t clientPid);
void serverHandler(int signal);
char * readLine(int fd);
int getrandom();
void initRandom();
void handleTimeout(int signal);
bool notBlockinglockFile(int fd);
bool unlockFile(int fd);
bool isFileUnlinked(char * filename);
char * getRequestExpression(char * num1, char *opr, char * num2);
char* getOperationSymbol(int opr);

// global variable that use to determine if a signal received from the server
// Type Declaration:
//      volatile
//             To ensure that the most current value is always read whenever it is accessed
//      sig_atomic_t
//              atomic variable that not be interrupted by signals
volatile sig_atomic_t receiveServerSignal = 0;

int main(int argc, char * argv[])
{
    pid_t clientPid, serverPid;
    int toServerFd;
    char pidBuffer[MAX_PID_LEN];
    char errorMsg[SNPRINTF_MAX_BUFFER_SIZE];
    int connectionAttempt = 0;
    int waitTime = 0;
    char * expression;

    initRandom();

    // Set up the signal handler for the server response
    signal(CLIENT_SIGNAL, serverHandler);

    // Set up the signal handler for SIGALRM
    signal(SIGALRM, handleTimeout);

    if(argc != ARGC_VALUE) {
        snprintf(errorMsg, sizeof(errorMsg), "The program needs exactly %d arguments\n", ARGC_VALUE -1);
        perror(errorMsg);
        exit(1);
    }

    for(int i = 1; i < ARGC_VALUE; i++){
        if(!isNumber(argv[i])) {
           snprintf(errorMsg, sizeof(errorMsg), "The argument \'%s\' is not a number\n", argv[i]);
            perror(errorMsg);
             exit(2);
        }
    }

    clientPid = getpid();
    if (snprintf(pidBuffer, sizeof(pidBuffer), "%d\n", clientPid) < 0) {
        perror("Failed to write to buffer, and convert the pid to sting");
    }

    // For parallel running of number of clients, wait before trying to connect
    waitTime = getrandom();
    printf("\n    DEBUG: client \'%d\' wait %d seconds before trying to connecting...\n", clientPid, waitTime);
    sleep(waitTime);

    // 10 attempts to connect to the server
    while( connectionAttempt < NUM_CONNECTIONS_ATTEMPTS) {

        // Open the file without O_TRUNC
        // So if the file is already locked by another client, the data will not be removed  
        toServerFd = open(TO_SERVER_FILE, O_WRONLY | O_CREAT, 0644);
        if(toServerFd < 0) {
            snprintf(errorMsg, sizeof(errorMsg), "Failed to open \'%s\' file\n", TO_SERVER_FILE);
            perror(errorMsg);
            exit(3);
        }

        // The file already locked by someone
        if(!notBlockinglockFile(toServerFd)) {
            close(toServerFd);
            waitTime = getrandom();
            printf("    DEBUG: Client \'%d\' attempt %d to connect to server failed, (failed to \'LOCK\' the file \'%s\'), wait %d seconds\n",clientPid, connectionAttempt+1, TO_SERVER_FILE, waitTime);
            sleep(waitTime);
        }

        else {
            // If the file removed
            if (isFileUnlinked(TO_SERVER_FILE)) {
                printf("    DEBUG: The \'%s\' file, has been unlinked by the server, so need to open it again\n", TO_SERVER_FILE);
                unlockFile(toServerFd);
                close(toServerFd);
            }
            else {
                // Success to acquire a lock and the file isn't removed
                printf("    DEBUG: Client \'%d\' lock the file \'%s\' at attempt %d\n", clientPid, TO_SERVER_FILE, connectionAttempt+1);
                break;
            }
        }
        connectionAttempt++;
    }

    if(connectionAttempt == NUM_CONNECTIONS_ATTEMPTS) {
        snprintf(errorMsg, sizeof(errorMsg), "Client \'%d\' Failed connect to server via \'%s\' file\n", clientPid, TO_SERVER_FILE);
        perror(errorMsg);
        exit(4);
    }

    // Clearing the toServer.txt file data
    if (ftruncate(toServerFd, 0) < 0 ) {
        snprintf(errorMsg, sizeof(errorMsg), "Failed clear \'%s\' file data\n", TO_SERVER_FILE);
        unlockFile(toServerFd);
        close(toServerFd);
        exit(5);
    }

    printf("Stage 3 - client \'%d\' connected to the server\n", clientPid);
    
    if (write(toServerFd, pidBuffer, strlen(pidBuffer) * sizeof(char)) != strlen(pidBuffer)* sizeof(char) ) {
        perror("Failed to write the pid to the file\n");
        unlockFile(toServerFd);
        close(toServerFd);
        exit(6);
    }

    // Write the arguments to the file
    for(int i = 2; i < ARGC_VALUE; i++) {
        // Write an argument
        if (write(toServerFd, argv[i], strlen(argv[i]) * sizeof(char)) != strlen(argv[i]) * sizeof(char)){
            snprintf(errorMsg, sizeof(errorMsg), "Failed to write the argument \'%s\' to a file\n", argv[i]);
            perror(errorMsg);
            unlockFile(toServerFd);
            close(toServerFd);
            exit(7);
        }

        // write '/n'
        if(i + 1 < ARGC_VALUE) {
            if (write(toServerFd, NEW_LINE, strlen(NEW_LINE) * sizeof(char)) != strlen(NEW_LINE) * sizeof(char)){
                snprintf(errorMsg, sizeof(errorMsg), "Failed to write \'%s\' to a file\n", NEW_LINE);
                perror(errorMsg);
                unlockFile(toServerFd);
                close(toServerFd);
                exit(8);
            }
        }
    }
    unlockFile(toServerFd);
    close(toServerFd);
    printf("Stage 4 - client \'%d\', writed to \'%s\' file\n", clientPid, TO_SERVER_FILE);

    serverPid = atoi(argv[1]);
    if (kill(serverPid, SERVER_SIGNAL) < 0 ) {
        perror("Faild to kill to the server\n");
        exit(9);
    }

    expression = getRequestExpression(argv[2], argv[3], argv[4]);
    printf("Stage 5 - client \'%d\' sent signal \'%d\' to the server, and waiting for the solution of: \'%s\'...\n    Server stages 6-15...\n\n", clientPid, SERVER_SIGNAL, expression);
    free(expression);

    // Set the initial alarm for 30 seconds
    alarm(TIMEOUT);
    pause(); // signal from server/timeout

    if (receiveServerSignal) {
        exit(0);
    }

    else {
        exit(13);
    }
}

bool isNumber(const char *str) {
    char *end;
    if (str == NULL) {
        perror("Failed to convert the string to int\n");
        return false;
    }

    errno = 0; // Reset errno before the call to strtol
    long l = strtol(str, &end, 10); // Base 10 for decimal conversion

    // Check for conversion errors
    if (end == str) {
        perror("No digits were found, conversion failed.\n");
        return false; 
    }
    else if (*end != '\0') {
        perror("Further characters after the number");
        return false;
    }

    // Check for any overflow/underflow
    if (errno == ERANGE && (l == LONG_MAX || l == LONG_MIN)) {
        perror("Integer value out of range.\n");
        return false;
    }

    return true;
}

char * getResponseFileName(pid_t clientPid) {
    char filename[SNPRINTF_MAX_BUFFER_SIZE];
    char pidBuffer[MAX_PID_LEN];

    if (snprintf(pidBuffer, sizeof(pidBuffer), "%d", clientPid) < 0) {
        perror("Failed to write to buffer, and convert the pid to sting");
        return NULL;
    }

    if (snprintf(filename, sizeof(filename), "toClient%s.txt", pidBuffer) < 0) {
        perror("Failed to write to buffer, and convert the pid to string");
        return NULL;
    }

    return strdup(filename);
}

void serverHandler(int signal) {
    
    char * fileNameResponse;
    int resultFd;
    char * resultLine;
    pid_t clientPid;

    receiveServerSignal = 1;
    alarm(0); // Cancel the pending alarm
    clientPid = getpid();

    printf("Stage 16 - client \'%d\' got signal from the server\n", clientPid);

    fileNameResponse = getResponseFileName(clientPid);

    resultFd = open(fileNameResponse, O_RDONLY);
    if(resultFd < 0) {
        perror("Faild to open the server response file\n");
        free(fileNameResponse);
        exit(10);
    }

    resultLine = readLine(resultFd);
    if(resultLine == NULL) {
        perror("Failed to read result from the file\n");
        close(resultFd);
        if (unlink(fileNameResponse) != 0) {
            perror("Failed to remove the server responsd file\n");
        }
        free(fileNameResponse);
        exit(11);
    }
    close(resultFd);

    printf("Stage 17 - client \'%d\' read the server response from the \'%s\' file\n", clientPid, fileNameResponse);
    printf("Stage 18 - Client \'%d\' the result is: %s\n", clientPid, resultLine);
    free(resultLine);

    // Remove the fileNameResponse file
    if (unlink(fileNameResponse) != 0) {
        perror("Failed to remove the server responsd file\n");
        free(fileNameResponse);
		exit(12);
    }

    printf("Stage 19 - client \'%d\' deleted \'%s\' file\n", clientPid, fileNameResponse);
    free(fileNameResponse);
}

// reads a string till the '\n' character from a given stream
char * readLine(int fd) {
	int errCheck;
	char * line = malloc(sizeof(char));
	char * temp = NULL;
	int size = 1;
	if (line == NULL) {
		perror("Error allocating memory\n");
		return NULL;
	}

	while (1) {
		errCheck = read(fd, line + size - 1, sizeof(char));
		if (errCheck < 0) {
			perror("Error reading from file\n");
			free(line);
			return NULL;
		}
		if (line[size - 1] == '\n') {
			line[size - 1] = '\0';
			break;
		}
		temp = realloc(line, (size + 1) * sizeof(char));
			if (temp == NULL) {
				perror("Error allocating memory\n");
				free(line);
				return NULL;
			}
		line = temp;
		size++;
	}

	return line;
}

void initRandom() {
    // In cases number of clients start at the same time
    // Use an identifier PID for srandom init (xor with current time)
    srandom(time(NULL) ^ getpid());
}

int getrandom() {
    return  (random() % 5) + 1;
}

// Signal handler for SIGALRM
void handleTimeout(int signal) {
    pid_t clientPid;
    if (signal == SIGALRM && receiveServerSignal == 0) {
        clientPid = getpid();
        printf("\nClient \'%d\' closed because no response was received from the server for %d seconds\n", clientPid, TIMEOUT);
    }
}

bool notBlockinglockFile(int fd) {
    // Exclusive lock, not blocking
    return flock(fd, LOCK_EX | LOCK_NB) == -1 ? false : true; 
}

bool unlockFile(int fd) {
    // Unlock the file
    return flock(fd, LOCK_UN) == -1 ? false : true;
}

bool isFileUnlinked(char * filename) {
    struct stat buf;
    if (stat(filename, &buf) < 0) {
        perror("Failed to stat file");
        return true; // Assume unlinked if stat fails
    }
    return (buf.st_nlink == 0); // No links to the file
}

char * getRequestExpression(char * num1, char *opr, char * num2) {
    char expression[MAX_EXPRESSION_LEN];
    char * symbol = NULL;
    strcpy(expression, num1);
    symbol = getOperationSymbol(atoi(opr));
    strcat(expression, symbol);
    free(symbol);
    strcat(expression, num2);
    return strdup(expression);
}

char* getOperationSymbol(int opr) {
    char symbol[MAX_SYMBOL_LEN];
    switch (opr) {
        case PLUS_OPERATION:
          strcpy(symbol, "+");
          break;
        case MINUS_OPERATION:
            strcpy(symbol, "-");
            break;
        case MUL_OPERATION:
            strcpy(symbol, "*");
            break;
        case DIV_OPERATION:
          strcpy(symbol, "/");
            break;
        default:
        strcpy(symbol, "?");
    }

    return strdup(symbol);
}