#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/file.h>

#define SNPRINTF_MAX_BUFFER_SIZE 285
#define SERVER_SIGNAL 1
#define CLIENT_SIGNAL 2
#define TO_SERVER_FILE "toServer.txt"
#define MAX_PID_LEN 10
#define TIMEOUT 60
#define PLUS_OPERATION 1
#define MINUS_OPERATION 2
#define MUL_OPERATION 3
#define DIV_OPERATION 4
#define SERVER_LOCK_WAIT_TIME 2.0

char * readLine(int fd);
void clientHandler(int signal);
double calc(int opr, double num1, double num2);
char * getResponseFileName(pid_t clientPid);
void handleTimeout(int signal);
bool blockingLockFile(int fd);
bool unlockFile(int fd);

volatile sig_atomic_t receiveClientSignal = 0;

int main(int argc, char * argv[])
{
    pid_t serverPid;

    // Set up the signal handler for the clients requests
    signal(SERVER_SIGNAL, clientHandler);

    // Set up the signal handler for SIGALRM
    signal(SIGALRM, handleTimeout);

    // Set the initial alarm for 60 seconds
    alarm(TIMEOUT);

    // doesnt matter if failed, if its failed the file doesn't existed
    unlink(TO_SERVER_FILE);
    printf("Stage 1 - Server deleted the \'%s\' file if existed\n", TO_SERVER_FILE);
    
    serverPid = getpid();
    printf("Stage 2 - Server is up and waiting for clients signals, for PID \'%d\' and SIGNAL \'%d\'...\n", serverPid, SERVER_SIGNAL);
    printf("    Client stages 3-5\n\n");

    while(1) {
        // It is possible to signals to collapased
        pause(); // wait for signal client/timeout
        if(receiveClientSignal == 1) {
            receiveClientSignal = 0;
            printf("Stage 8 - Server waiting for new clients...\n\n");
        }
        else{
            break;
        }
    }

    // Clear the zombies
    while(wait(NULL) != -1);
    exit(0);
}

// reads a string till the '\n' character from a given stream
char * readLine(int fd) {
	int errCheck;
	char * line = malloc(sizeof(char));
	char * temp = NULL;
	int size = 1;
	if (line == NULL) {
		perror("Error allocating memory\n");
		return NULL;
	}

	while (1) {
		errCheck = read(fd, line + size - 1, sizeof(char));
		if (errCheck < 0) {
			perror("Error reading from file\n");
			free(line);
			return NULL;
		}
		if (line[size - 1] == '\n') {
			line[size - 1] = '\0';
			break;
		}
		temp = realloc(line, (size + 1) * sizeof(char));
			if (temp == NULL) {
				perror("Error allocating memory\n");
				free(line);
				return NULL;
			}
		line = temp;
		size++;
	}

	return line;
}

void clientHandler(int signal) {
    int toServerFd;
    char * buffer;
    char convertResultBuf[SNPRINTF_MAX_BUFFER_SIZE];
    pid_t clientPid;
    double result, num1, num2;
    int opr;
    char * clientFileName;
    int toClientFd;
    int handleClientPid;
    int lockRetriesCount = 0;
    int isFileExist;
    char errorMsg[SNPRINTF_MAX_BUFFER_SIZE];

    receiveClientSignal = 1;
    alarm(TIMEOUT);

    printf("Stage 6 - Server got signal from a client\n");
    printf("Stage 7 - Server creates a child process to handle the client\n");

    handleClientPid = fork();

    // father process, on fork faliure
    if(handleClientPid < 0) {
        perror("Failed to create a child process\n");
        exit(1);
    }

    // child process
    if(handleClientPid == 0) {
        printf("    ----------------------------------------------------------------------------------------------------------\n    Stage 9 - child starts\n");

        // If the server got a signal, the child process will keep tying to lock the file
        while(1) {
            toServerFd = open(TO_SERVER_FILE, O_RDONLY);
            if(toServerFd < 0) {
                snprintf(errorMsg, sizeof(errorMsg), "Failed to open \'%s\' file\n", TO_SERVER_FILE);
                perror(errorMsg);
                exit(2);
            }

            // Blocking call to lock the file
            // Only when the file toServer.txt is unlocked the child try to lock the file
            if(!blockingLockFile(toServerFd)) {
                close(toServerFd);
                printf("        DEBUG: Attempt %d to \'LOCK\' the file \'%s\' failed, wait %lf seconds\n", lockRetriesCount+1, TO_SERVER_FILE, SERVER_LOCK_WAIT_TIME);
                lockRetriesCount++;
                sleep(SERVER_LOCK_WAIT_TIME);
            }

            else {
                printf("        DEBUG:\'LOCK\' the file \'%s\' at attempt %d\n", TO_SERVER_FILE, lockRetriesCount+1);
                break;
            }
        }

        // Read the content of toServer.txt file
        buffer = readLine(toServerFd);
        if(buffer == NULL){
            perror("Faild to read the client pid from the file\n");
            unlockFile(toServerFd);
            close(toServerFd);
            exit(3);
        }
        clientPid = atoi(buffer);
        free(buffer);
        printf("    Stage 10 - Server child process handles \'%d\' client\n", clientPid);

        buffer = readLine(toServerFd);
        if(buffer == NULL){
            perror("Faild to read the first number from the file\n");
            unlockFile(toServerFd);
            close(toServerFd);
            exit(4);
        }
        num1 = atof(buffer);
        free(buffer);
    
        buffer = readLine(toServerFd);
        if(buffer == NULL){
            perror("Faild to read the operation from the file\n");
            unlockFile(toServerFd);
            close(toServerFd);
            exit(5);
        }
        opr = atoi(buffer);
        free(buffer);
    
        buffer = readLine(toServerFd);
        if(buffer == NULL){
            perror("Faild to read the second number from the file\n");
            unlockFile(toServerFd);
            close(toServerFd);
            exit(6);
        }
        num2 = atof(buffer);
        free(buffer);

        // doesnt matter if failed, if its failed the file doesn't existed
        // for enable new clients to get service from the server
        // Importent to remove it before unlocking the file because this possible scenario:
        //      1. Child process unlock the file
        //      2. A new client lock the file and write to it
        //      3. The child process that handle the previous client will remove the file
        //      4. The child process that handle the new client will faild to open the file
        //         because the previous child delete the new client data and file
        
        isFileExist = unlink(TO_SERVER_FILE);   
        if(isFileExist < 0) {
            printf("        DEBUG: The \'%s\' file doesn't exists\n", TO_SERVER_FILE);
        }

        else {
            printf("    Stage 11 - Server child process that handels \'%d\' client, deleted \'%s\' file\n", clientPid, TO_SERVER_FILE);
        }
        
        // unlock the file and close it, finished reading the file
        unlockFile(toServerFd);
        close(toServerFd);

        // get the response file name - toClient<CLIENT_PID>.txt
        clientFileName = getResponseFileName(clientPid);
        if(clientFileName == NULL) {
            perror("Failed to get the response file name\n");
            exit(7);
        }

        // Open the toClient<CLIENT_PID>.txt file
        toClientFd = open(clientFileName, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if(toClientFd < 0) {
            perror("Faild to create new file for client reponse\n");
            free(clientFileName);
            exit(8);
        }
    
        // Calculate the client request
        if(opr == DIV_OPERATION && num2 == 0){
            strcpy(convertResultBuf, "Division by zero error");
        }
        else if( (opr != PLUS_OPERATION) && (opr != MINUS_OPERATION) &&
                (opr != MUL_OPERATION) && (opr != DIV_OPERATION) ) {
                strcpy(convertResultBuf, "No such operation");
        }
        else {
            result = calc(opr, num1, num2);
            snprintf(convertResultBuf, sizeof(convertResultBuf), "%.4lf", result);
        }

        printf("    Stage 12 - Server child process calculated \'%d\' client request\n", clientPid);

       // Write the result to the response file
        if (write(toClientFd, convertResultBuf, strlen(convertResultBuf) * sizeof(char)) != strlen(convertResultBuf) * sizeof(char)) {
            perror("Failed to write the result in the client file\n");
            free(clientFileName);
            close(toClientFd);
            exit(9);
        }

        printf("    Stage 13 - Server child process writed result of \'%d\' client request, to the \'%s\' file, which is: \'%s\'\n", clientPid, clientFileName, convertResultBuf);
        free(clientFileName);
        
        // kill to the client
        if (kill(clientPid, CLIENT_SIGNAL) < 0 ) {
            snprintf(errorMsg, sizeof(errorMsg), "Faild to kill to the \'%d\' client\n", clientPid);
            perror(errorMsg);
            exit(10);
        }
        printf("    Stage 14 - Server child process sent signal \'%d\' to \'%d\' client\n",CLIENT_SIGNAL, clientPid);
        printf("    Stage 15 - child finished\n    ----------------------------------------------------------------------------------------------------------\n\n");

        exit(0);
    }
}


double calc(int opr, double num1, double num2) {
    switch (opr) {
        case PLUS_OPERATION:
          return num1 + num2;
        case MINUS_OPERATION:
            return num1 - num2;
        case MUL_OPERATION:
            return num1 * num2;
        case DIV_OPERATION:
            return num1 / num2;
        default:
        perror("No such operation");
        exit(11);
    }
}

char * getResponseFileName(pid_t clientPid) {
    char filename[SNPRINTF_MAX_BUFFER_SIZE];
    char pidBuffer[MAX_PID_LEN];

    if (snprintf(pidBuffer, sizeof(pidBuffer), "%d", clientPid) < 0) {
        perror("Failed to write to buffer, and convert the pid to sting");
        return NULL;
    }

    if (snprintf(filename, sizeof(filename), "toClient%s.txt", pidBuffer) < 0) {
        perror("Failed to write to buffer, and convert the pid to string");
        return NULL;
    }

    return strdup(filename);
}

// Signal handler for SIGALRM
void handleTimeout(int signal) {
    if (signal == SIGALRM && receiveClientSignal == 0) {
        printf("\nThe server was closed because no service request was received for the last %d seconds\n", TIMEOUT);
    }
}


bool blockingLockFile(int fd) {
    // Exclusive lock
    return flock(fd, LOCK_EX ) == -1 ? false : true;
}


bool unlockFile(int fd) {
    // Unlock the file
    return flock(fd, LOCK_UN) == -1 ? false : true;
}
