#!/bin/bash

# Check if arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <CLIENT_EXECUTABLE_PATH> <SERVER_PID>"
    exit 1
fi

CLIENT_EXECUTABLE_PATH=$1
SERVER_PID=$2

# Run the client commands
$CLIENT_EXECUTABLE_PATH $SERVER_PID 10 4 9 &
$CLIENT_EXECUTABLE_PATH $SERVER_PID 12 4 5 &
$CLIENT_EXECUTABLE_PATH $SERVER_PID 9274 2 2191 &

exit 0