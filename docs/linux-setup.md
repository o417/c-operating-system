# Linux SetUp

## Create Linux Virtual Machine
Watch this youtube video
[Tutorial](https://youtu.be/nvdnQX9UkMY?si=jPDe0pw_3o89xg6R)<br/>
<br/>
Installation Files<br/>
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Ubuntu ISO](https://ubuntu.com/download/desktop)

## Install Needed Packages
Open the ubuntu terminal and switch to user root
```
ohad@ubuntu:~$ su root
Password: <the same as your user password>
```
* Install gcc - c compiler
    ```
    root@ubuntu:/home/ohad# apt install gcc
    ```
* Install git
    ```
    root@ubuntu:/home/ohad# apt install git
    ```

* Install network tools
    ```
    root@ubuntu:/home/ohad# apt install net-tools
    ```

* Install SSH Server
    ```
    root@ubuntu:/home/ohad# apt install openssh-server    
    ```

## Configure Firewall Rule for SSH Connections
Read this tutorial
[Remote SSH](https://www.makeuseof.com/how-to-ssh-into-virtualbox-ubuntu/)