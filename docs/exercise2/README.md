# Exercise 2
Signals Client Server Calculator

## Server Side

* The server waits for clients using pause() function
* When its gets signal it reset the server timer
* Create a child process to handle the client, the father waits for new clients
    * read from the toServer.txt file
    * Delete the toServer.txt file
    * It calculate the operation
    * Write to the toClient<CLINET_PID>.txt the result
    * kill to the client
* If the server get no request in 60 seconds its exit

### Running the code
* Compile
    ```
    gcc src/exercise2/ex2_srv.c -Wall -o src/exercise2/ex2_srv.out
    ```
* Run
    ```
    src/exercise2/ex2_srv.out
    echo $?
    ```

## Client Side

* The client gets in its arguments the server pid, two number and the operation
* Its write his pid, two numbers and operation to toServer.txt file
* Kill to the server
* start a timer
* waits for response using pause()
    * When it get signal from the server, he read the file toClient<CLIENT_PID>.txt
    * Remove the toClient<CLIENT_PID>.txt file
    * Prints the result
* If after 30 seconds the client get no response its exit

### Running the code
* Compile
    ```
    gcc src/exercise2/ex2_client.c -Wall -o src/exercise2/ex2_client.out
    ```
* Run
    ```
    ps -a
    src/exercise2/ex2_client.out
    echo $?
    ```