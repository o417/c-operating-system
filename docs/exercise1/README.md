# Exercise 1
This assignment has two parts.

### Part A - Compare Two Files
* This program gets two files as arguments.
* Its return 2 if they has the same content equals, otherwise return 1.
* <b> Run The Code</b>
    * Compile the code
        ```
        gcc ./src/exercise1/comp.c -Wall -o src/exercise1/comp.out
        ```
    * Run the code
        ```
        ./src/exercise1/comp.out <file_1> <file_2>
        ```
    * Print the return value
        ```
        echo $?
        ```
* Run Example
    * Equals Files
        ```
        ./src/exercise1/comp.out ./examples/exercise1/files-to-compare/ohad-desc1.txt ./examples/exercise1/files-to-compare/ohad-desc2.txt
        echo $?
        ```
    * Diffrent Files
        ```
        ./src/exercise1/comp.out ./examples/exercise1/files-to-compare/ohad-desc1.txt ./examples/exercise1/files-to-compare/shavit-desc1.txt
        echo $?
        ```
    * Not Exists File
        ```
        ./src/exercise1/comp.out ./examples/exercise1/files-to-compare/ohad-desc3.txt ./examples/exercise1/files-to-compare/shavit-desc1.txt
        echo $?
        ```

### Part B - Test System

This program gets a config file with 3 fields and give grades for students.

#### Infratracture
The given file is <b>config.txt</b> that contains 3 lines
* <b>The students folder</b> directory + name
* <b>The input file</b> directory + name
* <b>The expected output file</b> directory + name<br/>
<br/>
This is the ./examples/exrecise1 folder structure<br/>
<br/>
    ├── config.txt<br/>
    ├── intput.txt<br/>
    ├── output.txt<br/>
    ├── allStudents<br/>
    &emsp;&emsp;├── Moshe<br/>
    &emsp;&emsp;&emsp;&emsp;├── main.c<br/>
    &emsp;&emsp;&emsp;&emsp;├── main.exe<br/>
    &emsp;&emsp;├── Dan<br/>
    &emsp;&emsp;&emsp;&emsp;├── main.exe<br/><br/>

* For example, <b>running Moshe code</b><br/>
    * Compile
        ```
        gcc examples/exercise1/testSystem/allStudents/Moshe/main.c -Wall -o examples/exercise1/testSystem/allStudents/Moshe/main.exe
        ```
    * Run
        ```
        examples/exercise1/testSystem/allStudents/Moshe/main.exe
        ```

#### Running the code
* Compile
    ```
    gcc src/exercise1/grades-system.c -Wall -o src/exercise1/grades-system.out
    ```
* Run
    ```
    src/exercise1/grades-system.out examples/exercise1/testSystem/config.txt
    ```