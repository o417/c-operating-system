# VS Code IDE
I recommends to use VS Code as your IDE beacuse it has remote ssh exstention fo remote development

## SetUp
* Install VS Code IDE <br/>
[VS Code](https://code.visualstudio.com/download)<br/>

* Watch this tutorial to install and configure Remote SSH Exstention<br/>
    [Remote SSH Exstention](https://youtu.be/7kum46SFIaY?si=GF1VQnomBR_mZHPq)<br/>

    <b>Notice</b><br/>
    If you configure your virtual machine ssh server with another port, your ssh client command is diffrent.<br/>
    For example:
    ```ssh -p 2222 ohad@127.0.0.1```